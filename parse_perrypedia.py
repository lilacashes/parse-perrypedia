#!/usr/bin/env python3

from pprint import pprint
from argparse import ArgumentParser, Namespace
from collections import defaultdict
from datetime import date
from glob import glob
from html.parser import HTMLParser
from os.path import isfile
from pickle import dump, load
from sys import argv
from typing import List, Dict, Union

from ebooklib import epub
from lxml import etree, html
from requests import get

__author__ = 'Lene Preuss <lene.preuss@gmail.com>'


EPUB_BASE_DIR = '/home/lene/Music/spoken/Literature & Mythology/Perry Rhodan/' \
                'Perry Rhodan Epub Collection 1-2546'
MONTHS = [
    'Januar', 'Februar', 'März', 'April', 'Mai', 'Juni',
    'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'
]


def epub_for_novel(novel_number: int) -> str:
    subdir = "{hundreds:02d}{first}-{hundreds:02d}99".format(
        hundreds=(int(novel_number / 100)), first="00" if novel_number > 99 else "01"
    )
    files = glob("{}/{}/{:04d}*.epub".format(EPUB_BASE_DIR, subdir, novel_number))
    return files[0] if files else None


def url_for_novel(novel_number: int) -> str:
    return 'http://www.perrypedia.proc.org/wiki/Quelle:PR{}'.format(novel_number)


def print_xml(element: etree.Element) -> None:
    print(str(etree.tostring(element, pretty_print=True), 'utf-8'))


def extract_author(author_cell: etree.Element) -> str:
    return author_cell.find('a').text.replace('\xa0', ' ')


def strip_tags(html: str) -> str:

    class MLStripper(HTMLParser):
        def __init__(self):
            self.reset()
            self.strict = False
            self.convert_charrefs = True
            self.fed = []
            super().__init__()

        def handle_data(self, d):
            self.fed.append(d)

        def get_data(self):
            return ''.join(self.fed)

    s = MLStripper()
    s.feed(html)
    return s.get_data()


class EpubEdition:

    def __init__(self, book_number: int) -> None:
        epub_file = epub_for_novel(book_number)
        self._book = None
        if epub_file is not None:
            try:
                self._book = epub.read_epub(epub_file)
            except KeyError:
                pass

    def synopsis(self) -> Union[str, None]:
        if not self._book:
            return None

        for item in [i for i in self._book.get_items() if isinstance(i, epub.EpubHtml)]:
            elements = html.fromstring(item.content).xpath('body/p[@class="P-P2"]')
            elements = [e.text for e in elements if e.text is not None and len(e.text) > 10]
            if 1 <= len(elements) <= 20:
                return '\n'.join(elements)

        return None


class PerryRhodanPage:

    SAVE_FILE_NAME = 'novels.pickle'

    pages: List['PerryRhodanPage'] = []

    @classmethod
    def save(cls, pages: List):
        dump(pages, open(cls.SAVE_FILE_NAME, 'wb'))

    @classmethod
    def load(cls) -> List:
        return load(open(cls.SAVE_FILE_NAME, 'rb')) if isfile(cls.SAVE_FILE_NAME) else []

    @classmethod
    def generate(cls, start: int, end: int) -> None:
        cls.pages = cls.load()
        for number in range(max(start, len(cls.pages) + 1), end + 1):
            novel = cls(number)
            cls.pages.append(cls(number))
            print(number, novel.title, ' ' * 40, end='\r')
            if novel.synopsis:
                print('\n', novel.synopsis)
            cls.save(cls.pages)

    @classmethod
    def slice(cls, start: int, end: int) -> List:
        if not cls.pages:
            cls.generate(1, end)
        return cls.pages[start - 1:end]

    def __init__(self, novel_number: int) -> None:
        self.author: str = None
        self.publish_date: date = None
        self.number = novel_number
        html_page = get(url_for_novel(novel_number))
        content = etree.fromstring(html_page.text.encode('utf-8')).find('body/div[@id="content"]')
        self.title = self._read_title(content)

        body_content = content.find('div[@id="bodyContent"]')
        self._extract_overview_data(body_content)
        self.synopsis = self._read_synopsis(body_content) or \
            self._read_synopsis_from_epub(self.number)

    def __str__(self):
        return f"""Perry Rhodan {self.number}: {self.title} (Heftroman)
{self.author}
{self.publish_date}
""" + (f'{self.synopsis}\n' if self.synopsis else '') + \
               f'Pabel-Moewig Verlag KG, Rastatt'

    def _extract_overview_data(self, body_content: etree.Element) -> None:
        for row in self._overview_table_rows(body_content):
            cells = row.findall('td')
            if cells and cells[0] is not None and cells[0].text is not None:
                if 'Autor:' in cells[0].text:
                    self.author = extract_author(cells[1])
                elif 'Erstmals' in cells[0].text and 'erschienen' in cells[0].text:
                    self.publish_date = self._extract_date(cells[1])

    @staticmethod
    def _read_title(content: etree.Element) -> str:
        try:
            return content.find('h1/span').text.replace(' (Roman)', '')
        except AttributeError:
            return content.find('h1').text.replace(' (Roman)', '')

    @staticmethod
    def _extract_date(date_cell: etree.Element) -> date:
        date_text = strip_tags(etree.tostring(date_cell, encoding='unicode'))
        try:
            parts = date_text.strip().split()
            if ',' in parts[0]:
                parts = parts[1:]
            if len(parts) == 3:
                return date(
                    year=int(parts[2]), month=MONTHS.index(parts[1])+1, day=int(parts[0].strip('.'))
                )
            else:
                parts = date_text.strip().split()
                if len(parts) == 2:
                    return date(year=int(parts[1]), month=MONTHS.index(parts[0])+1, day=1)
                else:
                        return date(year=int(parts[0]), month=12, day=1)
        except ValueError:
            raise
            return None

    @staticmethod
    def _overview_table_rows(body_content: etree.Element) -> List[etree.Element]:
        overview_table = body_content.find('div/div[@class="perrypedia_std_rframe overview"]/table')
        return overview_table.findall('tr')

    @staticmethod
    def _read_synopsis(body_content: etree.Element) -> str:
        parse_next_p = False
        for div in body_content.findall('div'):
            for element in div.iter():
                if element.tag == 'h2':
                    span = element.find('span')
                    if span is not None and 'Kurzzusammenfassung' in span.text:
                        parse_next_p = True
                elif element.tag == 'p' and parse_next_p:
                    return strip_tags(str(etree.tostring(element), 'utf-8')).strip()
        return ''

    @staticmethod
    def _read_synopsis_from_epub(book_number: int) -> Union[str, None]:
        epub = EpubEdition(book_number)
        return epub.synopsis()


def count_authors(pages: List[PerryRhodanPage]) -> Dict[str, int]:
    authors: Dict[str, int] = defaultdict(int)
    for page in pages:
        authors[page.author] += 1
    return authors


def parse() -> Namespace:
    parser = ArgumentParser(description="Read Perrypedia and print info about Perry Rhodan issues")
    parser.add_argument('-s', '--start', default=1, type=int, help='First issue')
    parser.add_argument('-e', '--end', default=0, type=int, help='Last issue')
    parser.add_argument(
        '-g', '--goodreads', action='store_true', help='Print info required by goodreads'
    )
    return parser.parse_args(argv[1:])


def run(opts: Namespace):
    pages = PerryRhodanPage.slice(opts.start, opts.end if opts.end else opts.start)
    if opts.goodreads:
        for page in pages:
            print(str(page))
    else:
        print(len([page for page in pages if page.synopsis is not None]), 'with synopsis', ' ' * 40)

        authors = count_authors(pages)
        pprint(
            sorted([(a[0], a[1]) for a in authors.items()], key=lambda pair: pair[1], reverse=True)
        )


if __name__ == '__main__':
    run(parse())
